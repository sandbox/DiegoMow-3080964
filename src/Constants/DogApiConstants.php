<?php

namespace Drupal\the_dog_api\Constants;

/**
 * Class DogApiConstants.
 *
 * @package Drupal\the_dog_api\Constants
 */
class DogApiConstants {

  const MODULE_NAME = 'the_dog_api';

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = self::MODULE_NAME . '.settings';

  /**
   * Endpoints.
   *
   * @var string
   */
  const DOG_API_URL = 'https://api.thedogapi.com/v1';
  const GET_RANDOM_DOG_ENDPOINT = '/images/search';
  const GET_DOG_ENDPOINT = '/images/@ID';

  /**
   * API KEY Field.
   *
   * @var string
   */
  const API_KEY = 'x-api-key';

  /**
   * GET Image Param - Size.
   *
   * @var string
   */
  const GET_IMAGE_PARAM_SIZE = 'size';
  const GET_IMAGE_PARAM_SIZE_THUMB_VALUE = 'thumb';
  const GET_IMAGE_PARAM_SIZE_THUMB_LABEL = 'Thumbnail';
  const GET_IMAGE_PARAM_SIZE_SMALL_VALUE = 'small';
  const GET_IMAGE_PARAM_SIZE_SMALL_LABEL = 'Small';
  const GET_IMAGE_PARAM_SIZE_MEDIUM_VALUE = 'med';
  const GET_IMAGE_PARAM_SIZE_MEDIUM_LABEL = 'Medium';
  const GET_IMAGE_PARAM_SIZE_LARGE_VALUE = 'full';
  const GET_IMAGE_PARAM_SIZE_LARGE_LABEL = 'Large';
  const GET_IMAGE_PARAM_SIZE_DEFAULT_VALUE = self::GET_IMAGE_PARAM_SIZE_THUMB_VALUE;

  /**
   * Static function to render GET IMAGE PARAM SIZE OPTIONS.
   *
   * @return array
   *   Array with options.
   */
  public static function getImageParamSizeOptions() {
    return [
      DogApiConstants::GET_IMAGE_PARAM_SIZE_THUMB_VALUE => t(DogApiConstants::GET_IMAGE_PARAM_SIZE_THUMB_LABEL),
      DogApiConstants::GET_IMAGE_PARAM_SIZE_SMALL_VALUE => t(DogApiConstants::GET_IMAGE_PARAM_SIZE_SMALL_LABEL),
      DogApiConstants::GET_IMAGE_PARAM_SIZE_MEDIUM_VALUE => t(DogApiConstants::GET_IMAGE_PARAM_SIZE_MEDIUM_LABEL),
      DogApiConstants::GET_IMAGE_PARAM_SIZE_LARGE_VALUE => t(DogApiConstants::GET_IMAGE_PARAM_SIZE_LARGE_LABEL),
    ];
  }

}
