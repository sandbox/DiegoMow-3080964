<?php

namespace Drupal\the_dog_api\Element;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a render element to display a Dog.
 *
 * @RenderElement("dog")
 */
class Dog extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'dog',
      '#image' => NULL,
      '#id' => NULL,
      "#height" => '',
      "#width" => '',
    ];
  }

}
