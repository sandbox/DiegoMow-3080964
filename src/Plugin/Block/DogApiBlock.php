<?php

namespace Drupal\the_dog_api\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\the_dog_api\Service\DogApiRequest;
use Drupal\the_dog_api\Constants\DogApiConstants;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "dog_api_block",
 *   admin_label = @Translation("Dog Api Block"),
 * )
 */
class DogApiBlock extends BlockBase {

  /**
   * Default Key. Use for Unsetting Overrides at options.
   */
  const DEFAULT_KEY = 'default';

  /**
   * {@inheritdoc}
   */
  public function build() {
    $blockConfig = $this->getConfiguration();
    $blockParams = [];
    if (isset($blockConfig[DogApiConstants::GET_IMAGE_PARAM_SIZE]) && $blockConfig[DogApiConstants::GET_IMAGE_PARAM_SIZE] !== self::DEFAULT_KEY) {
      $blockParams[DogApiConstants::GET_IMAGE_PARAM_SIZE] = $blockConfig[DogApiConstants::GET_IMAGE_PARAM_SIZE];
    }
    $dogApi = new DogApiRequest();
    return $dogApi->getRandomDog($blockParams);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $blockConfig = $this->getConfiguration();
    $config = \Drupal::config(DogApiConstants::SETTINGS);
    $options = DogApiConstants::getImageParamSizeOptions();
    $default = $options[$config->get(DogApiConstants::GET_IMAGE_PARAM_SIZE, static::GET_IMAGE_PARAM_SIZE_DEFAULT_VALUE)];
    $default_option = [
      static::DEFAULT_KEY => $this->t('Default (@default)', ['@default' => $default]),
    ];
    $form[DogApiConstants::GET_IMAGE_PARAM_SIZE] = [
      '#type' => 'radios',
      '#title' => $this->t('Image Size'),
      '#required' => TRUE,
      '#options' => array_merge($default_option, $options),
      '#default_value' => isset($blockConfig[DogApiConstants::GET_IMAGE_PARAM_SIZE]) ? $blockConfig[DogApiConstants::GET_IMAGE_PARAM_SIZE] : 'default',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration[DogApiConstants::GET_IMAGE_PARAM_SIZE] = $form_state->getValue(DogApiConstants::GET_IMAGE_PARAM_SIZE);
  }
}
