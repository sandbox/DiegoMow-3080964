<?php

namespace Drupal\the_dog_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\the_dog_api\Constants\DogApiConstants;

/**
 * Defines a form that configures The Dog Api settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'the_dog_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      DogApiConstants::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(DogApiConstants::SETTINGS);

    $form[DogApiConstants::API_KEY] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('API key for usage with The Dog Api. Some services require a valid API Key to work.'),
      '#default_value' => $config->get(DogApiConstants::API_KEY, ''),
    ];

    $form[DogApiConstants::GET_IMAGE_PARAM_SIZE] = [
      '#type' => 'radios',
      '#title' => $this->t('Image Size'),
      '#description' => $this->t('Define the size of Images to load. Note: Sometimes it fails!'),
      '#required' => TRUE,
      '#options' => DogApiConstants::getImageParamSizeOptions(),
      '#default_value' => $config->get(DogApiConstants::GET_IMAGE_PARAM_SIZE, DogApiConstants::GET_IMAGE_PARAM_SIZE_DEFAULT_VALUE),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(DogApiConstants::SETTINGS)
      ->set(DogApiConstants::API_KEY, $form_state->getValue(DogApiConstants::API_KEY))
      ->set(DogApiConstants::GET_IMAGE_PARAM_SIZE, $form_state->getValue(DogApiConstants::GET_IMAGE_PARAM_SIZE))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
