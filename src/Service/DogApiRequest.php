<?php

namespace Drupal\the_dog_api\Service;

use Drupal\the_dog_api\Constants\DogApiConstants;

/**
 * Class DogApiRequest.
 *
 * @package Drupal\the_dog_api\Controller
 */
class DogApiRequest extends DogApiClass {

  /**
   * Load Params for requesting Dogs.
   *
   * @param array $overridedParams
   *   List with values to override defaul call.
   *
   * @return array
   *   Params array.
   */
  private function getRequestParams(array $overridedParams) {
    $params = [
      DogApiConstants::GET_IMAGE_PARAM_SIZE => $this->config->get(DogApiConstants::GET_IMAGE_PARAM_SIZE, DogApiConstants::GET_IMAGE_PARAM_SIZE_DEFAULT_VALUE),
    ];

    foreach ($overridedParams as $key => $value) {
      if ($value !== NULL) {
        $params[$key] = $value;
      }
    }

    return $params;
  }

  /**
   * Get a Random Dog Image.
   *
   * @param array $overridedParams
   *   List with values to override defaul call.
   *
   * @return string
   *   A html string with.
   */
  public function getRandomDog(array $overridedParams = []) {
    $params = $this->getRequestParams($overridedParams);
    $dogs = $this->call('GET', DogApiConstants::GET_RANDOM_DOG_ENDPOINT, $params);
    if (empty($dogs)) {
      return [
        '#markup' => '<strong>No Dog Found!</strong>'
      ];
    }
    $dog = array_shift($dogs);
    return [
      '#type' => 'dog',
      '#image' => $dog->url,
      '#id' => $dog->id,
      "#height" => $dog->height,
      "#width" => $dog->width,
    ];
  }

  /**
   * Get a Specific Dog Image.
   *
   * @param string $id
   *   Dog Id.
   *
   * @return string
   *   A html string with.
   */
  public function getDog($id) {
    $dog = $this->call('GET', str_replace('@ID', $id, DogApiConstants::GET_DOG_ENDPOINT));
    if (empty($dog)) {
      return [
        '#markup' => '<strong>No Dog Found!</strong>'
      ];
    }
    return [
      '#type' => 'dog',
      '#image' => $dog->url,
      '#id' => $dog->id,
      "#height" => $dog->height,
      "#width" => $dog->width,
    ];
  }

}
