<?php

namespace Drupal\the_dog_api\Service;

use Drupal\the_dog_api\Constants\DogApiConstants;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class DogApiClass.
 *
 * @package Drupal\the_dog_api\Controller
 */
abstract class DogApiClass {

  /**
   * Http Client for requests.
   */
  protected $http_client;

  /**
   * Configuration Variables.
   */
  protected $config;

  /**
   * Logger.
   */
  protected $logger;

  /**
   * Constructor of class.
   */
  public function __construct() {
    $this->http_client = \Drupal::httpClient();
    $this->config = \Drupal::config(DogApiConstants::SETTINGS);
    $this->logger = \Drupal::logger(DogApiConstants::MODULE_NAME);
  }

  /**
   * Convert array to Query String Param.
   *
   * @param array $params
   *   An array with key/value items.
   *
   * @return string
   *   A query string param.
   */
  protected function paramsToQueryString(array $params) {
    foreach ($params as $key => $value) {
      $queryStrings[] = $key . '=' . $value;
    }
    return '?' . implode('&', $queryStrings);
  }

  /**
   * Get Options for the request, such as headers.
   *
   * @return array
   *   Options for request.
   */
  protected function getApiOptions() {
    $options = [];
    $api_key = $this->config->get(DogApiConstants::API_KEY, '');
    if (!empty($api_key)) {
      $options['headers'][DogApiConstants::API_KEY] = $api_key;
    }
    return $options;
  }

  /**
   * Call Api.
   *
   * @param string $method
   *   API Call Method (GET, POST, DELETE, etc).
   * @param string $endpoint
   *   String with endpoint to be triggered.
   * @param array $params
   *   Query string parameters to be combined with endpoint.
   *
   * @return array
   *   Decoded Contentas Array.
   */
  protected function call($method, $endpoint, array $params = []) {
    $fullEndpoint = DogApiConstants::DOG_API_URL . $endpoint;
    if (!empty($params)) {
      $fullEndpoint .= $this->paramsToQueryString($params);
    }
    $options = $this->getApiOptions();
    try {
      $request = $this->http_client->request($method, $fullEndpoint, $options);
    }
    catch (GuzzleException $e) {
      // TODO: better error messages.
      $this->logger->error($e->getMessage());
      return [];
    }

    return json_decode($request->getBody()->getContents());
  }

}
